export class Urls{
    private static domain ='marialex.com';
    private static baseUrl:string = "http://localhost:8080/api/v1";

    static getBaseUrl():string{
        return this.baseUrl;
    }

    static getDomainName():string{
        return this.domain;
    }
}
