import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from '../components/login/login.component';
import { HomeComponent } from '../components/home/home.component';
import { SignupComponent } from '../components/signup/signup.component';
import { RetrivePasswordComponent } from '../components/retrive-password/retrive-password.component';
import { AboutComponent } from '../components/about/about.component';
import { TermsAndConditionsComponent } from '../components/terms-of-services/terms-of-services.component';
import { FeedsComponent } from '../components/feeds/feeds.component';
import { VideosComponent } from '../components/videos/videos.component';
import { PrivacyPolicyComponent } from '../components/privacy-policy/privacy-policy.component';
import { PageNotFoundComponent } from '../components/page-not-found/page-not-found.component';
import { MainComponent } from '../components/main/main.component';
import { Urls } from './constant-variables';
import { FeedsHomeComponent } from '../components/feeds/components/home/home.component';
import { FeedsMainComponent } from '../components/feeds/components/main/main.component';
import { SearchFeedsComponent } from '../components/feeds/components/search-feeds/search-feeds.component';

const routes: Routes = [
  {
    path:'',
    redirectTo: Urls.getDomainName(),
    pathMatch:'full'
  },
  {
    path: Urls.getDomainName(), 
    component: MainComponent,
    children:[
      {
        path:'',
        redirectTo:'home',
        pathMatch:'full'
      },
      { path:'home',
       component:HomeComponent
      },
      { path:'about',
       component:AboutComponent
      },
      { path:'login',
       component:LoginComponent
      },
      { path:'signup',
       component:SignupComponent
      },
      {
        path: 'privacy-policy', 
        component: PrivacyPolicyComponent 
      },
      {
        path: 'terms-of-services', 
        component: TermsAndConditionsComponent 
      },
      {
        path: 'retrive-password', 
        component: RetrivePasswordComponent 
      },
      {
        path: 'page-not-found', 
        component:PageNotFoundComponent 
      }
      
    ]
},
{
  path: Urls.getDomainName()+'/feeds', 
  component: FeedsComponent,
  children:[
    {
      path:'',
      component:FeedsMainComponent,
      children:[
        {
          path:'',
          redirectTo:'post',
          pathMatch:'full'
        },
        {
          path:'home',
          component:FeedsHomeComponent
        },
        {
          path:'search',
          component:SearchFeedsComponent
        }
      ]
    }
  ]
},
{
  path: Urls.getDomainName()+'/videos', 
  component: VideosComponent 
},
{
  path: '**', 
  redirectTo:Urls.getDomainName()+'/page-not-found',
  pathMatch:'full'
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}