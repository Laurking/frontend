import { Injectable } from '@angular/core';
import { Urls } from '../../config/constant-variables';
import {Http, Headers, RequestOptions,Response } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { Observable } from 'rxjs/Observable';
import { Post } from '../../entities/feeds/post';


@Injectable()
export class PostService {
   private baseUrl:string = Urls.getBaseUrl();
   private headers = new Headers({'content-type':'application/json'});
   private options = new RequestOptions({headers:this.headers});

  constructor(private httpService:Http) { }

  createPost(post:Post){
    return this.httpService
    .post(this.baseUrl+"/create-post",JSON.stringify(post),this.options)
    .map((response:Response)=>response.json())
    .catch(this.errorHnadler);
  }

  getAllPost(){
    return this.httpService
    .get(this.baseUrl+"/read-posts",this.options)
    .map((response:Response)=>response.json())
    .catch(this.errorHnadler);
  }

  getPost(postId:number){
    return this.httpService
    .get(this.baseUrl+"/read-post/"+postId,this.options)
    .map((response:Response)=>response.json())
    .catch(this.errorHnadler);
  }

  updatePost(post:Post){
    return this.httpService
    .put(this.baseUrl+"/update-post",JSON.stringify(post),this.options)
    .map((response:Response)=>response.json())
    .catch(this.errorHnadler);
  }

  deletePost(post:Post){
    return this.httpService
    .delete(this.baseUrl+"/delete-post/"+post.id,this.options)
    .map((response:Response)=>response.json())
    .catch(this.errorHnadler);
  }

  errorHnadler(error:Response){
    return Observable.throw(error || 'SERVER ERROR');
  }


}
