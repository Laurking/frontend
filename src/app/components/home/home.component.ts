import { Component } from '@angular/core';
import { JumbotronArray, Jumbotron } from '../../entities/jumbotron/jumbotron';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';
import { Urls } from '../../config/constant-variables';

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit{
  private index = 0;
  private jumbotronArray:JumbotronArray = new JumbotronArray();
  private jumboArray:Jumbotron[] = new Array();;
  private displayJumbo:Jumbotron;

  ngOnInit(){
    this.jumboArray = this.jumbotronArray.jumboValues;
    this.displayJumbo = this.jumboArray[0];
  }

  readMore(){
    this.index = this.index + 1;
    if(this.index >= this.jumboArray.length){
      this.index = 0;
    }
    this.displayJumbo = this.jumboArray[this.index];
    
    
  }

}
