import { Component, OnInit } from '@angular/core';
import { Post } from '../../../../entities/feeds/post';
import { PostService } from '../../../../services/feeds/post.service';
import { NgForm } from '@angular/forms/src/directives/ng_form';


@Component({
  selector: 'feeds-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class FeedsHomeComponent implements OnInit {
 
  private newPost:Post = new Post();
  private posts:Post[];

  constructor(private postService:PostService) {
    this.postThreadMethod();
   }

  ngOnInit() {
    setInterval(function(){ 
      this.postThreadMethod();
    }, 100);

  }

  OnCreatePost(){
    if(this.newPost.body !== "" || this.newPost != null ){
      this.postService.createPost(this.newPost).subscribe((data)=>{
      },(error)=>{
        console.log(error);
      })
      this.newPost.body ="";
    }
    this.postThreadMethod();
  }

  onDeletePost(post){
    this.postService.deletePost(post).subscribe((data)=>{
    },(error)=>{
      console.log(error);
    })
    this.newPost.body ="";
    this.postThreadMethod();
  }

  private postThreadMethod(){
    this.postService.getAllPost().subscribe((data)=>{
      this.posts = data;
    },(error)=>{
      console.log(error);
    })
  }

}
