import {ViewChild, Component } from '@angular/core';
import { trigger,state,style,transition,animate,keyframes } from '@angular/animations';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';
import { PostService } from '../../../../services/feeds/post.service';
import { Post } from '../../../../entities/feeds/post';




@Component({
  selector: 'feeds-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
  
  animations: [
    trigger('enterAnimation', [
      state('small', style({
        transform: 'scale(1)',
      })),
      state('large', style({
        transform: 'scale(1.2)',
      })),
      transition('small => large', animate('100ms ease-in')),
    ]),
  ]
  
})
export class NavbarComponent implements OnInit {
  
  private show:boolean = false;
  @ViewChild("searchResults") searchResults;
  private foundSearchArray:Post[] = new Array();
  private allPost:Post[] = new Array();
  private searchValue = "";
  private numberOfElementFound:number;
  private searchResultHover = false;
  
  constructor(private postService:PostService){
  
  }
  
  ngOnInit(){
    this.show = false;
  }
  
  
  toggleSideBar(){
    if(this.show == true){
      this.show = false;
    }
    else{
      this.show = true;
    }
  }
  
  onFocus(){
    this.searchResults.nativeElement.style.display="block";
    this.postService.getAllPost().subscribe(data=>{
      this.allPost = data;
    },error=>{
      console.log(error);
    })
    this.show = false;
  }
  onFocusOut(){
    if(!this.searchResultHover){
      this.searchResults.nativeElement.style.display="none";
    }
    
  }


  onMouseHover(){
    this.searchResultHover = true;
  }

  onMouseOut(){
    this.searchResultHover = false;
  }
  
  search(){
    this.foundSearchArray=[];
    for(let post of this.allPost){
      if(this.searchFilter(post,this.searchValue,this.foundSearchArray)){
        //this.foundSearchArray.push(post);
        console.log("laurent");
      }
      
    }
    this.numberOfElementFound = this.foundSearchArray.length;
  }
  
  private searchFilter(postBeingSearched:Post,searchedValue:string,foundElementContainer:Post[]):boolean{
    let post:string = postBeingSearched.body.toLocaleLowerCase();
    let lowerCaseSearchValue = searchedValue.toLocaleLowerCase();
    if(searchedValue == "" || searchedValue == null || searchedValue.length==0){
      return false;
    }
    if(post.startsWith(lowerCaseSearchValue) &&  !foundElementContainer.includes(postBeingSearched)){
        return true;
    } 
    else if(post.indexOf(searchedValue)>=0){
      return true;
    }
    return false;
  }
  

  // private onResize(event) {
  // 	let width = event.target.innerWidth;
  // 	this.animateBasedOnWindowSize(width);
  // }
  
  // private animateBasedOnWindowSize(width:number){
  // 	if(width >=768){
  // 		this.show = true;
  // 	}
  // 	else{
  // 		this.show = false;
  // 	}
  // }
  
  
  // hideIdentificationMenu(){
  // 	let width =  window.innerWidth;
  // 	this.animateBasedOnWindowSize(width);
  // }
  //  hideMainMenu(){
  // 	let width =  window.innerWidth;
  // 	this.animateBasedOnWindowSize(width);
  //  }
}
