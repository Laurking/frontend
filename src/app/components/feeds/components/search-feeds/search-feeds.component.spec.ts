import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchFeedsComponent } from './search-feeds.component';

describe('SearchFeedsComponent', () => {
  let component: SearchFeedsComponent;
  let fixture: ComponentFixture<SearchFeedsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchFeedsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchFeedsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
