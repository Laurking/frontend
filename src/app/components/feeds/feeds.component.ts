import { Component, OnInit } from '@angular/core';
import { Post } from '../../entities/feeds/post';
import { PostService } from '../../services/feeds/post.service';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
  selector: 'app-feeds',
  templateUrl: './feeds.component.html',
  styleUrls: ['./feeds.component.scss']
})
export class FeedsComponent implements OnInit, OnDestroy {
  
  
  ngOnInit() {
    
  }

  ngOnDestroy(): void {
    
  }

}
