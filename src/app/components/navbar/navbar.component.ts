import { Component } from '@angular/core';
import { trigger,state,style,transition,animate,keyframes } from '@angular/animations';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';
import { Urls } from '../../config/constant-variables';



@Component({
	selector: 'navbar',
	templateUrl: './navbar.component.html',
	styleUrls: ['./navbar.component.scss'],
	
	animations: [
		trigger(
			'enterAnimation', [
				transition(':enter', [
					style({transform: 'translateX(100%)', opacity: 0}),
					animate('400ms', style({transform: 'translateX(0)', opacity: 1}))
				]),
				transition(':leave', [
					style({transform: 'translateX(0)', opacity: 1}),
					animate('400ms', style({transform: 'translateX(100%)', opacity: 0}))
				])
			]
		)
		
	]
	
})

export class NavigationBar implements OnInit{
	
	private show:boolean = false;
	private domainName:string = Urls.getDomainName();
	
	ngOnInit(){
		let width =  window.innerWidth;
		this.animateBasedOnWindowSize(width);
	}
	
	private onResize(event) {
		let width = event.target.innerWidth;
		this.animateBasedOnWindowSize(width);
	}
	
	private animateBasedOnWindowSize(width:number){
		if(width >=768){
			this.show = true;
		}
		else{
			this.show = false;
		}
	}


	hideIdentificationMenu(){
		let width =  window.innerWidth;
		this.animateBasedOnWindowSize(width);
	}
	 hideMainMenu(){
		let width =  window.innerWidth;
		this.animateBasedOnWindowSize(width);
	 }
}
