import { Comment } from "./comment";

export class Post {
    id:number;
    body:string;
    postDate:string;
    updateDate:string;
    comments:Comment[];
}
