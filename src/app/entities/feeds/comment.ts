import { Post } from "./post";
import { Reply } from "./reply";

export class Comment{
    id:number;
    body:string;
    postDate:string;
    updateDate:string;
    post:Post;
    replies:Reply[];
}