import { Comment } from "./comment";

export class Reply{
    id:number;
    body:string;
    postDate:string;
    updateDate:string;
    comment:Comment;
}