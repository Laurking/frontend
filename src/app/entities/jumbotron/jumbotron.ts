export class Jumbotron {
    title:string;
    body:string;
    constructor(title:string, body:string){
        this.title = title;
        this.body = body;
    }
}

export class JumbotronArray{
    jumboValues = [
        new Jumbotron(
            "What is Lorem Ipsum?",
            `Schema theory explains how our previous experiences,
             knowledge, emotions, and understandings affect what and
              how we learn (Harvey & Goudvis, 2000). Schema is the 
              background knowledge and experience readers bring 
              to the text`
        ),
        new Jumbotron(
            "Why do we use it?",
            `It is a long established fact that a reader will be
             distracted by the readable content of a page when looking 
             at its layout. The point of using Lorem Ipsum is that it
              has a more`
        ),

        new Jumbotron(
            "Where can I get some?",
            `Lorem Ipsum is simply dummy text of the printing
            and typesetting industry. Lorem Ipsum has been the
            industry's standard dummy text ever since the 1500s`
        )
    ];
}
