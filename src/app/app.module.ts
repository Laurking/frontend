import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppRoutingModule } from './config/routing.module'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { NavigationBar } from './components/navbar/navbar.component';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { SignupComponent } from './components/signup/signup.component';
import { RetrivePasswordComponent } from './components/retrive-password/retrive-password.component';
import { AboutComponent } from './components/about/about.component';
import { TermsAndConditionsComponent } from './components/terms-of-services/terms-of-services.component';
import { VideosComponent } from './components/videos/videos.component';
import { PrivacyPolicyComponent } from './components/privacy-policy/privacy-policy.component';
import { FooterComponent } from './components/footer/footer.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { FeedsComponent } from './components/feeds/feeds.component';
import { PostService } from './services/feeds/post.service';
import { MainComponent } from './components/main/main.component';
import { FeedsHomeComponent } from './components/feeds/components/home/home.component';
import { NavbarComponent } from './components/feeds/components/navbar/navbar.component';
import { SidebarComponent } from './components/feeds/components/sidebar/sidebar.component';
import { FeedsMainComponent } from './components/feeds/components/main/main.component';
import { SearchFeedsComponent } from './components/feeds/components/search-feeds/search-feeds.component';

@NgModule({
  declarations: [
    AppComponent,
    NavigationBar,
    LoginComponent,
    HomeComponent,
    SignupComponent,
    RetrivePasswordComponent,
    AboutComponent,
    TermsAndConditionsComponent,
    VideosComponent,
    PrivacyPolicyComponent,
    FooterComponent,
    PageNotFoundComponent,

    
    FeedsComponent,
    FeedsMainComponent,
    SearchFeedsComponent,
    
    MainComponent,
    FeedsHomeComponent,
    NavbarComponent,
    SidebarComponent,
    SearchFeedsComponent,
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    BrowserAnimationsModule
  ],
  providers: [PostService],
  bootstrap: [AppComponent]
})
export class AppModule { }
